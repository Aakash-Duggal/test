const http = require("http");
const express = require("express");
require("./common/conn");
const swaggerUi = require("swagger-ui-express");
const YAML = require("yamljs");
const services = require("./services/index");

const socialRouter = require("./v1/routes/index");

const app = express();
app.use(express.json());

//--------------socket-----------------------------------------------
const server = http.createServer(app);
const io = require("socket.io")(server);
//-------------------------------------------------------------------


app.use("/api/v1/social", socialRouter);  //routing path

const swaggerDocument = YAML.load("./swagger/collection.yml");
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));



// server.listen(5000);

server.listen(5000, () => {
    // socket.SocketService(io);
    services.Swagger.createSwagger();
  });