const user = require("../../models/userModel");

const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;
const bcrypt = require("bcryptjs");
const { response } = require("express");
const jwt = require("jsonwebtoken");
const joi = require("joi");

const validations = require("../validations/validation");
const services = require("../../services/index");

//----------------------Registering User----------------------------
exports.signUpUser = async (req, res) => {
  try {
    // await validations.userValidationSchema.validateAsync(req.body);
    const { error } = validations.userValidationSchema.validate(req.body);

    const existingUser = await user.findOne({ email: req.body.email });
    if (existingUser) {
      return res.status(400).json({ message: "Email is already registered." });
    }

    // const result = await user.create(req.body);
    const newUser = {
      userName: req.body.userName,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      password: req.body.password,
      profilePicture: req.body.profilePicture || "", 
      bio: req.body.bio || "", 
    };

    const result = await user.create(newUser);

    //----------Sending Email On Registration------------------------
      const sendToMail=req.body.email;
      services.SendMail(sendToMail);

    console.log("User registered successfully:", result);
    return res.status(201).json({ message: "User registered successfully", user: result });
   
  } catch (error) {
    if (error.isJoi) {
      console.error("Joi validation error:", error.message);
      return res.status(400).send("Bad Request");
    }

    console.error("Error while registering user:", error);
    return res.status(500).send("Internal Server Error");
  }
};

//-------------------------------Generating Random JTI Here------------------------------------------------
const generateJTI = () => {
  const length = 25;
  const characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  let jti = "";

  for (let i = 0; i < length; i++) {
    const randomIndex = Math.floor(Math.random() * characters.length);
    jti += characters.charAt(randomIndex);
  }

  return jti;
};

//----------------------User Login---------------------------------------------------------------
exports.login = async (req, res) => {
  try {
    const { email, password } = req.body;

    await validations.emailLoginSchema.validateAsync({ email, password });

    const existingUser = await user.findOne({ email });

    if (!existingUser) {
      return res.status(404).send("User not found");
    }

    // Compairing the provided password with the hashed password in the database using compare method
    const isPasswordValid = await bcrypt.compare(
      password,
      existingUser.password
    );

    if (!isPasswordValid) {
      return res.status(401).send("Invalid password");
    }

    const jti = generateJTI(); //generate JTI function declared above
    const token = jwt.sign(
      { userId: existingUser._id, jti },
      "your-secret-key",
      { expiresIn: "1h" }
    );

    existingUser.jti = jti;
    await existingUser.save(); //saving the jti in the database

    return res.json({ token });
  } catch (error) {
    if (error.isJoi) {
      console.error("Joi validation error:", error.message);
      return res.status(400).send("Bad Request");
    }

    console.error("Error during login check:", error);
    return res.status(500).send("Internal Server Error");
  }
};

//------------------------Get Data------------------------------------------------------------------------------------------------
exports.getProfile = async (req, res) => {
  try {
    const token = req.headers.authorization;

    if (!token) {
      return res.status(401).json({ message: "Missing authorization token" });
    }
    const decodedToken = jwt.verify(token, "your-secret-key");

    const userId = decodedToken.userId;

    const userDetails = await user.findById(userId);

    if (!userDetails) {
      return res.status(404).json({ message: "User not found" });
    }

    const UserDetails = {
      userName: userDetails.userName,
      firstName: userDetails.firstName,
      lastName: userDetails.lastName,
      email: userDetails.email,
      profilePicture: userDetails.profilePicture,
      bio: userDetails.bio,
    };

    return res.json(UserDetails);
  } catch (error) {
    if (error.name === "JsonWebTokenError") {
      return res.status(401).json({ message: "Invalid token" });
    }

    console.error("Error fetching user details:", error);
    return res.status(500).send("Internal Server Error");
  }
};

//------------------------Update User Profile---------------------------------------------------------
exports.updateUserProfile = async (req, res) => {
  try {
    const token = req.headers.authorization;

    if (!token) {
      return res.status(401).json({ message: "Missing authorization token" });
    }

    const decodedToken = jwt.verify(token, "your-secret-key");

    const userId = decodedToken.userId;

    const existingUser = await user.findById(userId);

    if (!existingUser) {
      return res.status(404).json({ message: "User not found" });
    }

    existingUser.userName = req.body.userName || existingUser.userName;
    existingUser.firstName = req.body.firstName || existingUser.firstName;
    existingUser.lastName = req.body.lastName || existingUser.lastName;
    existingUser.email = req.body.email || existingUser.email;
    existingUser.profilePicture =
      req.body.profilePicture || existingUser.profilePicture;
    existingUser.bio = req.body.bio || existingUser.bio;

    const updatedUser = await existingUser.save();

    const UserDetails = {
      userName: updatedUser.userName,
      firstName: updatedUser.firstName,
      lastName: updatedUser.lastName,
      email: updatedUser.email,
      profilePicture: updatedUser.profilePicture,
      bio: updatedUser.bio,
    };

    console.log(UserDetails);
    return res.json(UserDetails);
  } catch (error) {
    if (error.name === "JsonWebTokenError") {
      return res.status(401).json({ message: "Invalid token" });
    }

    console.error("Error updating user profile:", error);
    return res.status(500).send("Internal Server Error");
  }
};

//------------------------Suspend User Profile---------------------------------------------------------
exports.deleteUserProfile = async (req, res) => {
  try {
    const token = req.headers.authorization; 

    if (!token) {
      return res.status(401).json({ message: "Missing authorization token" });
    }
    const decodedToken = jwt.verify(token, "your-secret-key"); 

    const userId = decodedToken.userId;

    const result = await user.updateOne({ _id: userId }, { $set: { isActive: false } });

    if (result.nModified === 0) {
      return res.status(404).json({ message: "User not found" });
    }

    const updatedUser = await user.findById(userId);

    const UserDetails = {
      userName: updatedUser.userName,
      firstName: updatedUser.firstName,
      lastName: updatedUser.lastName,
      email: updatedUser.email,
      profilePicture: updatedUser.profilePicture,
      bio: updatedUser.bio,
      isActive: updatedUser.isActive,
    };

    return res.json(UserDetails);
  } catch (error) {
    if (error.name === "JsonWebTokenError") {
      return res.status(401).json({ message: "Invalid token" });
    }

    console.error("Error suspending user profile:", error);
    return res.status(500).send("Internal Server Error");
  }
};

