const Blog = require("../../models/blogPostModel");
const Comment = require("../../models/commentModel");
const Reaction = require("../../models/likesModel");
const mongoose = require("mongoose");
const { response } = require("express");
const jwt = require("jsonwebtoken");
const joi = require("joi");

const validations = require("../validations/validation");


//------------------Creating Blog-----------------------------------------
exports.createBlog = async (req, res) => {
  try {
    await validations.blogPostValidationSchema.validateAsync(req.body);

    const token = req.headers.authorization;

    if (!token) {
      return res.status(401).json({ message: "Missing authorization token" });
    }

    const decodedToken = jwt.verify(token, "your-secret-key");

    const userId = decodedToken.userId;

    const newBlog = await Blog.create({
      author: userId,
      title: req.body.title,
      content: req.body.content,
      image: req.body.image,
    });

    const blog = {
      _id: newBlog._id,
      title: newBlog.title,
      content: newBlog.content,
      image: newBlog.image,
      createdAt: newBlog.createdAt,
    };

    return res
      .status(201)
      .json({ message: "Blog created successfully", blog: blog });
  } catch (error) {
    if (error.isJoi) {
      console.error("Joi validation error:", error.message);
      return res.status(400).send("Bad Request");
    }

    if (error.name === "JsonWebTokenError") {
      return res.status(401).json({ message: "Invalid token" });
    }

    console.error("Error creating blog:", error);
    return res.status(500).send("Internal Server Error");
  }
};

//--------------------------------Blog Comment--------------------------------------------------
exports.createComment = async (req, res) => {
  try {
    await validations.commentValidationSchema.validateAsync(req.body);

    const token = req.headers.authorization;

    if (!token) {
      return res.status(401).json({ message: "Missing authorization token" });
    }

    const decodedToken = jwt.verify(token, "your-secret-key");

    const userId = decodedToken.userId;

    const newComment = await Comment.create({
      user: userId,
      blogPost: req.body.blogPost,
      text: req.body.text,
    });

    const comment = {
      _id: newComment._id,
      user: newComment.user,
      blogPost: newComment.blogPost,
      text: newComment.text,
      createdAt: newComment.createdAt,
    };

    return res
      .status(201)
      .json({ message: "Comment created successfully", comment: comment });
  } catch (error) {
    if (error.isJoi) {
      console.error("Joi validation error:", error.message);
      return res.status(400).send("Bad Request");
    }

    if (error.name === "JsonWebTokenError") {
      return res.status(401).json({ message: "Invalid token" });
    }

    console.error("Error creating comment:", error);
    return res.status(500).send("Internal Server Error");
  }
};

//----------------------Blog Like-------------------------------------------
exports.blogReaction = async (req, res) => {
  try {
    await validations.reactionValidationSchema.validateAsync(req.body);

    const token = req.headers.authorization;
    if (!token) {
      return res.status(401).json({ message: "Missing authorization token" });
    }
    const decodedToken = jwt.verify(token, "your-secret-key");
    const userId = decodedToken.userId;

    const existingReaction = await Reaction.findOne({
      user: userId,
      blogPost: req.body.blogPost,
    });

    if (existingReaction) {
      existingReaction.like = req.body.like;
      existingReaction.dislike = req.body.dislike;
      await existingReaction.save();
      return res.status(200).json({ message: "Reaction updated successfully" });
    }

    // If the user has not reacted create a new reaction
    const newReaction = await Reaction.create({
      user: userId,
      blogPost: req.body.blogPost,
      like: req.body.like,
      dislike: req.body.dislike,
    });

    return res.status(201).json({
      message: "Reaction created successfully",
      reaction: newReaction,
    });
  } catch (error) {
    if (error.isJoi) {
      console.error("Joi validation error:", error.message);
      return res.status(400).send("Bad Request");
    }

    if (error.name === "JsonWebTokenError") {
      return res.status(401).json({ message: "Invalid token" });
    }

    console.error("Error creating reaction:", error);
    return res.status(500).send("Internal Server Error");
  }
};

//----------------------Get All Blog Posts by a User with Pagination------------------------
exports.getPaginatedBlogs = async (req, res) => {
    try {
      const token = req.headers.authorization;
      if (!token) {
        return res.status(401).json({ message: "Missing authorization token" });
      }
  
      const decodedToken = jwt.verify(token, "your-secret-key");
      const userId = decodedToken.userId;
  
      const page = parseInt(req.query.page) || 1; 
      const pageSize = parseInt(req.query.pageSize) || 10; 
  
      const skip = (page - 1) * pageSize;
  
      const blogs = await Blog.find({ author: userId })
        .skip(skip)
        .limit(pageSize)
        .sort({ createdAt: -1 }); 
  
      return res.status(200).json({ blogs: blogs });
    } catch (error) {
      if (error.name === "JsonWebTokenError") {
        return res.status(401).json({ message: "Invalid token" });
      }
  
      console.error("Error fetching blogs by user:", error);
      return res.status(500).send("Internal Server Error");
    }
  };
  


//----------------------Get All Blog Posts by a User INcluding Likes and Comments----------------------------------
exports.getAllBlogsByUser = async (req, res) => {
  try {
    const token = req.headers.authorization;
    if (!token) {
      return res.status(401).json({ message: "Missing authorization token" });
    }

    const decodedToken = jwt.verify(token, "your-secret-key");
    const userId = decodedToken.userId;

    // Use aggregation to get blogs with likes, dislikes, and comments count
    const blogs = await Blog.aggregate([
      {
        $match: { author: mongoose.Types.ObjectId(userId) },
      },
      {
        $lookup: {
          from: "likes",
          localField: "_id",
          foreignField: "blogPost",
          as: "reactions",
        },
      },
      {
        $lookup: {
          from: "comments",
          localField: "_id",
          foreignField: "blogPost",
          as: "comments",
        },
      },
      {
        $project: {
          _id: 1,
          title: 1,
          content: 1,
          image: 1,
          createdAt: 1,
          likes: { $sum: { $ifNull: ["$reactions.like", 0] } },
          dislikes: { $sum: { $ifNull: ["$reactions.dislike", 0] } },
          commentsCount: { $size: "$comments" },
        },
      },
    ]);

    return res.status(200).json({ blogs: blogs });
  } catch (error) {
    if (error.name === "JsonWebTokenError") {
      return res.status(401).json({ message: "Invalid token" });
    }

    console.error("Error fetching blogs by user:", error);
    return res.status(500).send("Internal Server Error");
  }
};
