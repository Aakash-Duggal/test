const Joi = require("joi");

const userValidationSchema = Joi.object({
  userName: Joi.string().required(),
  firstName: Joi.string().required(),
  lastName: Joi.string().required(),
  email: Joi.string().email().required(),
  password: Joi.string().required(),
});

const emailLoginSchema = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().required(),
});

const blogPostValidationSchema = Joi.object({
  author: Joi.string(),
  title: Joi.string().required(),
  content: Joi.string().required().max(100000),
  image: Joi.string(),
});

const commentValidationSchema = Joi.object({
  user: Joi.string(),
  blogPost: Joi.string().required(), 
  text: Joi.string().required(),
});

const reactionValidationSchema = Joi.object({
  user: Joi.string(), 
  blogPost: Joi.string().required(), 
  like: Joi.number(),
  dislike: Joi.number(),
});

// module.exports = {userValidationSchema};
module.exports = {
  userValidationSchema,
  emailLoginSchema,
  blogPostValidationSchema,
  commentValidationSchema,
  reactionValidationSchema 
};
