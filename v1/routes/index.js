const express = require("express");
const router = express.Router();
const blog = require("./blogRoutes");
const user = require("./userRoutes");


router.use("/user", user);
router.use("/blog", blog);


module.exports = router;