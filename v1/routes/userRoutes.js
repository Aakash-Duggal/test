const express = require("express");
const userController = require("../controllers/userController");
const router = express.Router();
const path = require("path");

//--------------Route To Register the user----------------------------------
router.route("/register").post(userController.signUpUser);

//--------------Route To Login the User----------------------------------
router.route("/login").post(userController.login);

//--------------Get user Profile Data----------------------------------
router.route("/getProfile").get(userController.getProfile);

//--------------Update User Profile Data----------------------------------
router.route("/updateProfile").patch(userController.updateUserProfile);

//---------------Temporary delete USer--------------------------------------------
router.route("/deleteProfile").delete(userController.deleteUserProfile);

module.exports = router;
