const express = require("express");
const blogController = require("../controllers/blogController");
const router = express.Router();
const path = require("path");

//--------------Create Blog----------------------------------
router.route("/createBlog").post(blogController.createBlog);

//---------------Comment On Blog------------------------------
router.route("/comment").post(blogController.createComment);

//-------------Like/Dislike On Blog---------------------------
router.route("/reaction").post(blogController.blogReaction);

//-------------Get All Blogs Details by a user-------------------------
router.route("/getBlogs").get(blogController.getAllBlogsByUser);

//-------------Getting all Blogs used pagination-----------------------------
router.route("/paginatedBlogs").get(blogController.getPaginatedBlogs);

module.exports = router;
