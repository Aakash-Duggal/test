const mongoose = require("mongoose");

const commentSchema = new mongoose.Schema({
  blogPost: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "blogs",
    required: true,
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "userdetails",
    required: true,
  },
  text: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

const Comment = mongoose.model("Comment", commentSchema);

module.exports = Comment;
