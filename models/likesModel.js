const mongoose = require("mongoose");

// const reactionSchema = new mongoose.Schema({
//   user: {
//     type: mongoose.Schema.Types.ObjectId,
//     ref: "User",
//     required: true,
//   },
//   blogPost: {
//     type: mongoose.Schema.Types.ObjectId,
//     ref: "BlogPost",
//     required: true,
//   },
//   like: {
//     type: Boolean,
//     default: false,
//   },
//   dislike: {
//     type: Boolean,
//     default: false,
//   },
//   createdAt: {
//     type: Date,
//     default: Date.now,
//   },
// });

const reactionSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  blogPost: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "BlogPost",
    required: true,
  },
  like: {
    type: Number,
    default: 0, 
  },
  createdAt: {
    type: Date,
    default: Date.now,
  }, // timestamps:true
});

const likes = mongoose.model("likes", reactionSchema);

module.exports = likes;

